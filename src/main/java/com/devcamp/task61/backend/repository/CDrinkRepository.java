package com.devcamp.task61.backend.repository;


import com.devcamp.task61.backend.model.CDrink;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CDrinkRepository extends JpaRepository<CDrink, Long> {
    
}

