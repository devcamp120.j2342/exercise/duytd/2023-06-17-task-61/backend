package com.devcamp.task61.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task61.backend.model.CMenu;

public interface CMunuRepository extends  JpaRepository<CMenu, Long> {
    
}
