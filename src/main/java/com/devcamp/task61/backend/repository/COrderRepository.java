package com.devcamp.task61.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task61.backend.model.COrder;

public interface COrderRepository extends JpaRepository<COrder, Long> {
    COrder findProductByOrderId(long orderId);
}
