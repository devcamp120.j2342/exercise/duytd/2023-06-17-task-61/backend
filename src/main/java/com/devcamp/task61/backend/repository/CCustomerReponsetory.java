package com.devcamp.task61.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task61.backend.model.CCustomer;

public interface CCustomerReponsetory extends JpaRepository<CCustomer, Long> {
    CCustomer findOrderByCustomerId(long customerId);
}
