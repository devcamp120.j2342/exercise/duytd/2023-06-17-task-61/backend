package com.devcamp.task61.backend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.task61.backend.model.CVoucher;

public interface CVoucherRepository extends JpaRepository<CVoucher, Long> {
    
}
