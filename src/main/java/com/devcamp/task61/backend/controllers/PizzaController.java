package com.devcamp.task61.backend.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.task61.backend.model.CCustomer;
import com.devcamp.task61.backend.model.CDrink;
import com.devcamp.task61.backend.model.CMenu;
import com.devcamp.task61.backend.model.COrder;
import com.devcamp.task61.backend.model.CVoucher;
import com.devcamp.task61.backend.model.Product;
import com.devcamp.task61.backend.repository.CCustomerReponsetory;
import com.devcamp.task61.backend.repository.CDrinkRepository;
import com.devcamp.task61.backend.repository.CMunuRepository;
import com.devcamp.task61.backend.repository.COrderRepository;
import com.devcamp.task61.backend.repository.CVoucherRepository;

import java.util.*;

@CrossOrigin
@RestController
public class PizzaController {

    @Autowired
    CVoucherRepository voucherRepository;
    @Autowired
    CDrinkRepository drinkRepository;
    @Autowired
    CMunuRepository menuRepository;
    @Autowired
    CCustomerReponsetory customerRepository;
    @Autowired
    COrderRepository orderRepository;

    @GetMapping("/vouchers")
    public ResponseEntity<List<CVoucher>> getAllVouchers() {
        try {
            List<CVoucher> vouchers = voucherRepository.findAll();
            return new ResponseEntity<>(vouchers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/drinks")
    public ResponseEntity<List<CDrink>> getAllDrink() {
        try {
            List<CDrink> drinks = new ArrayList<>();

            drinkRepository.findAll().forEach(drinks::add);
            return new ResponseEntity<>(drinks, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/menus")
    public ResponseEntity<List<CMenu>> getAllMenus() {
        try {
            List<CMenu> menus = new ArrayList<>();

            menuRepository.findAll().forEach(menus::add);
            return new ResponseEntity<>(menus, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/customers")
    public ResponseEntity<List<CCustomer>> getAllCustomers() {
        try {
            List<CCustomer> customers = new ArrayList<>();
            customerRepository.findAll().forEach(customers::add);
            return new ResponseEntity<>(customers, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/devcamp-orders")
    public ResponseEntity<List<COrder>> getOrderByCustomerId(@RequestParam(value="customerId") long customerId) {
        try {
            CCustomer customers = customerRepository.findOrderByCustomerId(customerId);
            if (customers != null) {
                List<COrder> order = new ArrayList<>();
                order.addAll(customers.getOrders());
                return new ResponseEntity<>(order, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/devcamp-product")
    public ResponseEntity<List<Product>> getAllProducts(@RequestParam(value="orderId") long orderId) {
        try {
            COrder orders  = orderRepository.findProductByOrderId(orderId);
            if(orders != null) {
                List<Product> products = new ArrayList<>();
                products.addAll(orders.getProducts());
                return new ResponseEntity<>(products, HttpStatus.OK);
            }else {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }
        } catch(Exception e) {
             return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
